# BOC-2 - Project Summary of Genia <!--subproject name-->

<!--Provide a *concise* description of your subproject, that helps other subgrantees to quickly determine what value there is if they work together with you, and/or let their results interact and interop with your results.-->

Genia is a personal information exchange that empowers people living with long-term illness, supports co-production of care, and generates valuable health data in patient-controlled information flows.

For more information, please contact: andreas.hager@genia.se

## Problem Statement

Clinical trials have been the standard for showing value of treatments. So-called real-world data (RWD) is a new important source for further value demonstration of health technologies, eg to assess the effectiveness on individual patient level or follow-up of reimbursement restrictions or pay for performance pricing schemes.

The Sweden Coalition CF mission is to be the leading Swedish example of introduction of personalized precision medicine for optimal health. Extraction of patientlevel RWD for this mission is possible today, but the main challenge is to create good datasets when data is captured in silos governed by different parties. 

Solutions are needed that: 

* overcome hurdles to integration and consolidate data silos

* collect new patient reported data to enrich existing data,

* automate data extraction for research and other purposes such as the near real-time improvement of healthcare and follow-up of therapies

* engage patients and healthcare professionals.

## Solution Description

Adding SSI capability will make Genia offering scalable and possible to build an eco-system using standard protocols. Applying internationally well-known model of registry-based learning networks in chronic care and shows how this model can innovatively form assurance communities.

Our solution is for managing and modelling data by partnering
with an innovative four-step-approach:

1. Formulate the healthcare data schema. Partner with stakeholders to develop information resource library (e.g. for better follow-up of antibiotic treatments in cystic fibrosis care). An open-source system for transparent data modelling is applied. 

2. Apply formulation to a data exchange workflow. SSI technology is applied to connect external RWD sources (eg Swedish e-health authority prescription database and SpiroHome home spirometry service) and to issue new patient generated credentials in app. Data is made available in a personal data store.

3. Use data store to manage data from sources (formulation, aggregation
and analysis) and re-issue credentials as needed. Include automated data agreements to ensure the solution is human-centric, ethical/lawful, quality
assured and fully auditable.

4. Design instant feed-back loops for patients and clinicians to ensure engagement.

